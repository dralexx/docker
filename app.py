from flask import Flask, escape, request

app = Flask(__name__)

@app.route('/')
def hello():
    return f'hello friends'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
